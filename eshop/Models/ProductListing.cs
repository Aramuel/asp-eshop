﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.Models
{
    public class ProductListing
    {
        public List<manufacturer> manufacturers = new List<manufacturer>();
        public List<option> options = new List<option>();
        public List<category> categories = new List<category>();
        public List<v_product_previews> product_previews = new List<v_product_previews>();

        public ProductListing(List<manufacturer> man, List<option> op, List<category> cat)
        {
            options = op;
            manufacturers = man;
            categories = cat;
        }
    }
}