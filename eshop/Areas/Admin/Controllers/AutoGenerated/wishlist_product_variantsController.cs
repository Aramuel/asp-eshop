﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class wishlist_product_variantsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/wishlist_product_variants
        public async Task<ActionResult> Index()
        {
            var wishlist_product_variants = db.wishlist_product_variants.Include(w => w.product_variants).Include(w => w.wishlist);
            return View(await wishlist_product_variants.ToListAsync());
        }

        // GET: Admin/wishlist_product_variants/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wishlist_product_variants wishlist_product_variants = await db.wishlist_product_variants.FindAsync(id);
            if (wishlist_product_variants == null)
            {
                return HttpNotFound();
            }
            return View(wishlist_product_variants);
        }

        // GET: Admin/wishlist_product_variants/Create
        public ActionResult Create()
        {
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description");
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name");
            return View();
        }

        // POST: Admin/wishlist_product_variants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,wishlist_id,product_id")] wishlist_product_variants wishlist_product_variants)
        {
            if (ModelState.IsValid)
            {
                db.wishlist_product_variants.Add(wishlist_product_variants);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", wishlist_product_variants.product_id);
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", wishlist_product_variants.wishlist_id);
            return View(wishlist_product_variants);
        }

        // GET: Admin/wishlist_product_variants/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wishlist_product_variants wishlist_product_variants = await db.wishlist_product_variants.FindAsync(id);
            if (wishlist_product_variants == null)
            {
                return HttpNotFound();
            }
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", wishlist_product_variants.product_id);
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", wishlist_product_variants.wishlist_id);
            return View(wishlist_product_variants);
        }

        // POST: Admin/wishlist_product_variants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,wishlist_id,product_id")] wishlist_product_variants wishlist_product_variants)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wishlist_product_variants).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", wishlist_product_variants.product_id);
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", wishlist_product_variants.wishlist_id);
            return View(wishlist_product_variants);
        }

        // GET: Admin/wishlist_product_variants/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            wishlist_product_variants wishlist_product_variants = await db.wishlist_product_variants.FindAsync(id);
            if (wishlist_product_variants == null)
            {
                return HttpNotFound();
            }
            return View(wishlist_product_variants);
        }

        // POST: Admin/wishlist_product_variants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            wishlist_product_variants wishlist_product_variants = await db.wishlist_product_variants.FindAsync(id);
            db.wishlist_product_variants.Remove(wishlist_product_variants);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
