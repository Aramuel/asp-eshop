﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_optionsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_options
        public async Task<ActionResult> Index()
        {
            var product_options = db.product_options.Include(p => p.option).Include(p => p.product);
            return View(await product_options.ToListAsync());
        }

        // GET: Admin/product_options/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_options product_options = await db.product_options.FindAsync(id);
            if (product_options == null)
            {
                return HttpNotFound();
            }
            return View(product_options);
        }

        // GET: Admin/product_options/Create
        public ActionResult Create()
        {
            ViewBag.option_id = new SelectList(db.options, "id", "value");
            ViewBag.product_id = new SelectList(db.products, "id", "description");
            return View();
        }

        // POST: Admin/product_options/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,option_id")] product_options product_options)
        {
            if (ModelState.IsValid)
            {
                db.product_options.Add(product_options);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.option_id = new SelectList(db.options, "id", "value", product_options.option_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_options.product_id);
            return View(product_options);
        }

        // GET: Admin/product_options/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_options product_options = await db.product_options.FindAsync(id);
            if (product_options == null)
            {
                return HttpNotFound();
            }
            ViewBag.option_id = new SelectList(db.options, "id", "value", product_options.option_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_options.product_id);
            return View(product_options);
        }

        // POST: Admin/product_options/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,option_id")] product_options product_options)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_options).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.option_id = new SelectList(db.options, "id", "value", product_options.option_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_options.product_id);
            return View(product_options);
        }

        // GET: Admin/product_options/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_options product_options = await db.product_options.FindAsync(id);
            if (product_options == null)
            {
                return HttpNotFound();
            }
            return View(product_options);
        }

        // POST: Admin/product_options/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_options product_options = await db.product_options.FindAsync(id);
            db.product_options.Remove(product_options);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
