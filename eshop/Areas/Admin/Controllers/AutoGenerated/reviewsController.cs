﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class reviewsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/reviews
        public async Task<ActionResult> Index()
        {
            var reviews = db.reviews.Include(r => r.product_variants).Include(r => r.user);
            return View(await reviews.ToListAsync());
        }

        // GET: Admin/reviews/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            review review = await db.reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // GET: Admin/reviews/Create
        public ActionResult Create()
        {
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description");
            ViewBag.user_id = new SelectList(db.users, "id", "email");
            return View();
        }

        // POST: Admin/reviews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,user_id,product_variant_id,value,positive,negative,creation_date")] review review)
        {
            if (ModelState.IsValid)
            {
                db.reviews.Add(review);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", review.product_variant_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", review.user_id);
            return View(review);
        }

        // GET: Admin/reviews/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            review review = await db.reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", review.product_variant_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", review.user_id);
            return View(review);
        }

        // POST: Admin/reviews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,user_id,product_variant_id,value,positive,negative,creation_date")] review review)
        {
            if (ModelState.IsValid)
            {
                db.Entry(review).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", review.product_variant_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", review.user_id);
            return View(review);
        }

        // GET: Admin/reviews/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            review review = await db.reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // POST: Admin/reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            review review = await db.reviews.FindAsync(id);
            db.reviews.Remove(review);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
