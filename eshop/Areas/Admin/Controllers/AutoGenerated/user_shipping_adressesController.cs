﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class user_shipping_adressesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/user_shipping_adresses
        public async Task<ActionResult> Index()
        {
            var user_shipping_adresses = db.user_shipping_adresses.Include(u => u.address).Include(u => u.user);
            return View(await user_shipping_adresses.ToListAsync());
        }

        // GET: Admin/user_shipping_adresses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_shipping_adresses user_shipping_adresses = await db.user_shipping_adresses.FindAsync(id);
            if (user_shipping_adresses == null)
            {
                return HttpNotFound();
            }
            return View(user_shipping_adresses);
        }

        // GET: Admin/user_shipping_adresses/Create
        public ActionResult Create()
        {
            ViewBag.adress_id = new SelectList(db.addresses, "id", "street");
            ViewBag.user_id = new SelectList(db.users, "id", "email");
            return View();
        }

        // POST: Admin/user_shipping_adresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,user_id,adress_id")] user_shipping_adresses user_shipping_adresses)
        {
            if (ModelState.IsValid)
            {
                db.user_shipping_adresses.Add(user_shipping_adresses);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.adress_id = new SelectList(db.addresses, "id", "street", user_shipping_adresses.adress_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_shipping_adresses.user_id);
            return View(user_shipping_adresses);
        }

        // GET: Admin/user_shipping_adresses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_shipping_adresses user_shipping_adresses = await db.user_shipping_adresses.FindAsync(id);
            if (user_shipping_adresses == null)
            {
                return HttpNotFound();
            }
            ViewBag.adress_id = new SelectList(db.addresses, "id", "street", user_shipping_adresses.adress_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_shipping_adresses.user_id);
            return View(user_shipping_adresses);
        }

        // POST: Admin/user_shipping_adresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,user_id,adress_id")] user_shipping_adresses user_shipping_adresses)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user_shipping_adresses).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.adress_id = new SelectList(db.addresses, "id", "street", user_shipping_adresses.adress_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_shipping_adresses.user_id);
            return View(user_shipping_adresses);
        }

        // GET: Admin/user_shipping_adresses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_shipping_adresses user_shipping_adresses = await db.user_shipping_adresses.FindAsync(id);
            if (user_shipping_adresses == null)
            {
                return HttpNotFound();
            }
            return View(user_shipping_adresses);
        }

        // POST: Admin/user_shipping_adresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            user_shipping_adresses user_shipping_adresses = await db.user_shipping_adresses.FindAsync(id);
            db.user_shipping_adresses.Remove(user_shipping_adresses);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
