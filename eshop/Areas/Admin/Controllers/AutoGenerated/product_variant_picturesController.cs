﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_variant_picturesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_variant_pictures
        public async Task<ActionResult> Index()
        {
            var product_variant_pictures = db.product_variant_pictures.Include(p => p.picture).Include(p => p.product_variants);
            return View(await product_variant_pictures.ToListAsync());
        }

        // GET: Admin/product_variant_pictures/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variant_pictures product_variant_pictures = await db.product_variant_pictures.FindAsync(id);
            if (product_variant_pictures == null)
            {
                return HttpNotFound();
            }
            return View(product_variant_pictures);
        }

        // GET: Admin/product_variant_pictures/Create
        public ActionResult Create()
        {
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name");
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description");
            return View();
        }

        // POST: Admin/product_variant_pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,picture_id,priority")] product_variant_pictures product_variant_pictures)
        {
            if (ModelState.IsValid)
            {
                db.product_variant_pictures.Add(product_variant_pictures);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", product_variant_pictures.picture_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", product_variant_pictures.product_id);
            return View(product_variant_pictures);
        }

        // GET: Admin/product_variant_pictures/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variant_pictures product_variant_pictures = await db.product_variant_pictures.FindAsync(id);
            if (product_variant_pictures == null)
            {
                return HttpNotFound();
            }
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", product_variant_pictures.picture_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", product_variant_pictures.product_id);
            return View(product_variant_pictures);
        }

        // POST: Admin/product_variant_pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,picture_id,priority")] product_variant_pictures product_variant_pictures)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_variant_pictures).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", product_variant_pictures.picture_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", product_variant_pictures.product_id);
            return View(product_variant_pictures);
        }

        // GET: Admin/product_variant_pictures/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variant_pictures product_variant_pictures = await db.product_variant_pictures.FindAsync(id);
            if (product_variant_pictures == null)
            {
                return HttpNotFound();
            }
            return View(product_variant_pictures);
        }

        // POST: Admin/product_variant_pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_variant_pictures product_variant_pictures = await db.product_variant_pictures.FindAsync(id);
            db.product_variant_pictures.Remove(product_variant_pictures);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
