﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_variantsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_variants
        public async Task<ActionResult> Index()
        {
            var product_variants = db.product_variants.Include(p => p.picture).Include(p => p.product);
            return View(await product_variants.ToListAsync());
        }

        // GET: Admin/product_variants/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variants product_variants = await db.product_variants.FindAsync(id);
            if (product_variants == null)
            {
                return HttpNotFound();
            }
            return View(product_variants);
        }

        // GET: Admin/product_variants/Create
        public ActionResult Create()
        {
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name");
            ViewBag.product_id = new SelectList(db.products, "id", "description");
            return View();
        }

        // POST: Admin/product_variants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,size_chart_picture_id,description,name,price,vat,quantity,expiration_date,creation_date,rating")] product_variants product_variants)
        {
            if (ModelState.IsValid)
            {
                db.product_variants.Add(product_variants);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product_variants.size_chart_picture_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_variants.product_id);
            return View(product_variants);
        }

        // GET: Admin/product_variants/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variants product_variants = await db.product_variants.FindAsync(id);
            if (product_variants == null)
            {
                return HttpNotFound();
            }
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product_variants.size_chart_picture_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_variants.product_id);
            return View(product_variants);
        }

        // POST: Admin/product_variants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,size_chart_picture_id,description,name,price,vat,quantity,expiration_date,creation_date,rating")] product_variants product_variants)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_variants).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product_variants.size_chart_picture_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_variants.product_id);
            return View(product_variants);
        }

        // GET: Admin/product_variants/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variants product_variants = await db.product_variants.FindAsync(id);
            if (product_variants == null)
            {
                return HttpNotFound();
            }
            return View(product_variants);
        }

        // POST: Admin/product_variants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_variants product_variants = await db.product_variants.FindAsync(id);
            db.product_variants.Remove(product_variants);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
