﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class blogsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/blogs
        public async Task<ActionResult> Index()
        {
            var blogs = db.blogs.Include(b => b.picture).Include(b => b.user);
            return View(await blogs.ToListAsync());
        }

        // GET: Admin/blogs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = await db.blogs.FindAsync(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // GET: Admin/blogs/Create
        public ActionResult Create()
        {
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name");
            ViewBag.user_id = new SelectList(db.users, "id", "email");
            return View();
        }

        // POST: Admin/blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,user_id,picture_id,title,preview,content,creation_date")] blog blog)
        {
            if (ModelState.IsValid)
            {
                db.blogs.Add(blog);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", blog.picture_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", blog.user_id);
            return View(blog);
        }

        // GET: Admin/blogs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = await db.blogs.FindAsync(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", blog.picture_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", blog.user_id);
            return View(blog);
        }

        // POST: Admin/blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,user_id,picture_id,title,preview,content,creation_date")] blog blog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.picture_id = new SelectList(db.pictures, "id", "name", blog.picture_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", blog.user_id);
            return View(blog);
        }

        // GET: Admin/blogs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = await db.blogs.FindAsync(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Admin/blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            blog blog = await db.blogs.FindAsync(id);
            db.blogs.Remove(blog);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
