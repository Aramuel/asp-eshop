﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class user_wishlistsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/user_wishlists
        public async Task<ActionResult> Index()
        {
            var user_wishlists = db.user_wishlists.Include(u => u.wishlist).Include(u => u.user);
            return View(await user_wishlists.ToListAsync());
        }

        // GET: Admin/user_wishlists/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_wishlists user_wishlists = await db.user_wishlists.FindAsync(id);
            if (user_wishlists == null)
            {
                return HttpNotFound();
            }
            return View(user_wishlists);
        }

        // GET: Admin/user_wishlists/Create
        public ActionResult Create()
        {
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name");
            ViewBag.user_id = new SelectList(db.users, "id", "email");
            return View();
        }

        // POST: Admin/user_wishlists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,user_id,wishlist_id")] user_wishlists user_wishlists)
        {
            if (ModelState.IsValid)
            {
                db.user_wishlists.Add(user_wishlists);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", user_wishlists.wishlist_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_wishlists.user_id);
            return View(user_wishlists);
        }

        // GET: Admin/user_wishlists/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_wishlists user_wishlists = await db.user_wishlists.FindAsync(id);
            if (user_wishlists == null)
            {
                return HttpNotFound();
            }
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", user_wishlists.wishlist_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_wishlists.user_id);
            return View(user_wishlists);
        }

        // POST: Admin/user_wishlists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,user_id,wishlist_id")] user_wishlists user_wishlists)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user_wishlists).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.wishlist_id = new SelectList(db.wishlists, "id", "name", user_wishlists.wishlist_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", user_wishlists.user_id);
            return View(user_wishlists);
        }

        // GET: Admin/user_wishlists/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_wishlists user_wishlists = await db.user_wishlists.FindAsync(id);
            if (user_wishlists == null)
            {
                return HttpNotFound();
            }
            return View(user_wishlists);
        }

        // POST: Admin/user_wishlists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            user_wishlists user_wishlists = await db.user_wishlists.FindAsync(id);
            db.user_wishlists.Remove(user_wishlists);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
