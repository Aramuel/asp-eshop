﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class shipping_typesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/shipping_types
        public async Task<ActionResult> Index()
        {
            return View(await db.shipping_types.ToListAsync());
        }

        // GET: Admin/shipping_types/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shipping_types shipping_types = await db.shipping_types.FindAsync(id);
            if (shipping_types == null)
            {
                return HttpNotFound();
            }
            return View(shipping_types);
        }

        // GET: Admin/shipping_types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/shipping_types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,description,rate_modifier")] shipping_types shipping_types)
        {
            if (ModelState.IsValid)
            {
                db.shipping_types.Add(shipping_types);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(shipping_types);
        }

        // GET: Admin/shipping_types/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shipping_types shipping_types = await db.shipping_types.FindAsync(id);
            if (shipping_types == null)
            {
                return HttpNotFound();
            }
            return View(shipping_types);
        }

        // POST: Admin/shipping_types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,description,rate_modifier")] shipping_types shipping_types)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shipping_types).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(shipping_types);
        }

        // GET: Admin/shipping_types/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shipping_types shipping_types = await db.shipping_types.FindAsync(id);
            if (shipping_types == null)
            {
                return HttpNotFound();
            }
            return View(shipping_types);
        }

        // POST: Admin/shipping_types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            shipping_types shipping_types = await db.shipping_types.FindAsync(id);
            db.shipping_types.Remove(shipping_types);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
