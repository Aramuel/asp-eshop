﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class order_product_variantsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/order_product_variants
        public async Task<ActionResult> Index()
        {
            var order_product_variants = db.order_product_variants.Include(o => o.order).Include(o => o.product_variants);
            return View(await order_product_variants.ToListAsync());
        }

        // GET: Admin/order_product_variants/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_product_variants order_product_variants = await db.order_product_variants.FindAsync(id);
            if (order_product_variants == null)
            {
                return HttpNotFound();
            }
            return View(order_product_variants);
        }

        // GET: Admin/order_product_variants/Create
        public ActionResult Create()
        {
            ViewBag.order_id = new SelectList(db.orders, "id", "id");
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description");
            return View();
        }

        // POST: Admin/order_product_variants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,order_id")] order_product_variants order_product_variants)
        {
            if (ModelState.IsValid)
            {
                db.order_product_variants.Add(order_product_variants);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_product_variants.order_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", order_product_variants.product_id);
            return View(order_product_variants);
        }

        // GET: Admin/order_product_variants/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_product_variants order_product_variants = await db.order_product_variants.FindAsync(id);
            if (order_product_variants == null)
            {
                return HttpNotFound();
            }
            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_product_variants.order_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", order_product_variants.product_id);
            return View(order_product_variants);
        }

        // POST: Admin/order_product_variants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,order_id")] order_product_variants order_product_variants)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_product_variants).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_product_variants.order_id);
            ViewBag.product_id = new SelectList(db.product_variants, "id", "description", order_product_variants.product_id);
            return View(order_product_variants);
        }

        // GET: Admin/order_product_variants/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_product_variants order_product_variants = await db.order_product_variants.FindAsync(id);
            if (order_product_variants == null)
            {
                return HttpNotFound();
            }
            return View(order_product_variants);
        }

        // POST: Admin/order_product_variants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            order_product_variants order_product_variants = await db.order_product_variants.FindAsync(id);
            db.order_product_variants.Remove(order_product_variants);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
