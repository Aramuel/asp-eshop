﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class blog_commentsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/blog_comments
        public async Task<ActionResult> Index()
        {
            var blog_comments = db.blog_comments.Include(b => b.blog).Include(b => b.comment);
            return View(await blog_comments.ToListAsync());
        }

        // GET: Admin/blog_comments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_comments blog_comments = await db.blog_comments.FindAsync(id);
            if (blog_comments == null)
            {
                return HttpNotFound();
            }
            return View(blog_comments);
        }

        // GET: Admin/blog_comments/Create
        public ActionResult Create()
        {
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title");
            ViewBag.comment_id = new SelectList(db.comments, "id", "text");
            return View();
        }

        // POST: Admin/blog_comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,blog_id,comment_id")] blog_comments blog_comments)
        {
            if (ModelState.IsValid)
            {
                db.blog_comments.Add(blog_comments);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_comments.blog_id);
            ViewBag.comment_id = new SelectList(db.comments, "id", "text", blog_comments.comment_id);
            return View(blog_comments);
        }

        // GET: Admin/blog_comments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_comments blog_comments = await db.blog_comments.FindAsync(id);
            if (blog_comments == null)
            {
                return HttpNotFound();
            }
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_comments.blog_id);
            ViewBag.comment_id = new SelectList(db.comments, "id", "text", blog_comments.comment_id);
            return View(blog_comments);
        }

        // POST: Admin/blog_comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,blog_id,comment_id")] blog_comments blog_comments)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog_comments).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_comments.blog_id);
            ViewBag.comment_id = new SelectList(db.comments, "id", "text", blog_comments.comment_id);
            return View(blog_comments);
        }

        // GET: Admin/blog_comments/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_comments blog_comments = await db.blog_comments.FindAsync(id);
            if (blog_comments == null)
            {
                return HttpNotFound();
            }
            return View(blog_comments);
        }

        // POST: Admin/blog_comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            blog_comments blog_comments = await db.blog_comments.FindAsync(id);
            db.blog_comments.Remove(blog_comments);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
