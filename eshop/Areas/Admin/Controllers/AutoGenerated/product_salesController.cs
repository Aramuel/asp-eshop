﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_salesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_sales
        public async Task<ActionResult> Index()
        {
            var product_sales = db.product_sales.Include(p => p.sale).Include(p => p.product_variants);
            return View(await product_sales.ToListAsync());
        }

        // GET: Admin/product_sales/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_sales product_sales = await db.product_sales.FindAsync(id);
            if (product_sales == null)
            {
                return HttpNotFound();
            }
            return View(product_sales);
        }

        // GET: Admin/product_sales/Create
        public ActionResult Create()
        {
            ViewBag.sale_id = new SelectList(db.sales, "id", "name");
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description");
            return View();
        }

        // POST: Admin/product_sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_variant_id,sale_id")] product_sales product_sales)
        {
            if (ModelState.IsValid)
            {
                db.product_sales.Add(product_sales);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.sale_id = new SelectList(db.sales, "id", "name", product_sales.sale_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_sales.product_variant_id);
            return View(product_sales);
        }

        // GET: Admin/product_sales/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_sales product_sales = await db.product_sales.FindAsync(id);
            if (product_sales == null)
            {
                return HttpNotFound();
            }
            ViewBag.sale_id = new SelectList(db.sales, "id", "name", product_sales.sale_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_sales.product_variant_id);
            return View(product_sales);
        }

        // POST: Admin/product_sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_variant_id,sale_id")] product_sales product_sales)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_sales).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.sale_id = new SelectList(db.sales, "id", "name", product_sales.sale_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_sales.product_variant_id);
            return View(product_sales);
        }

        // GET: Admin/product_sales/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_sales product_sales = await db.product_sales.FindAsync(id);
            if (product_sales == null)
            {
                return HttpNotFound();
            }
            return View(product_sales);
        }

        // POST: Admin/product_sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_sales product_sales = await db.product_sales.FindAsync(id);
            db.product_sales.Remove(product_sales);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
