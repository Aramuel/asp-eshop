﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class productsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/products
        public async Task<ActionResult> Index()
        {
            var products = db.products.Include(p => p.manufacturer).Include(p => p.picture);
            return View(await products.ToListAsync());
        }

        // GET: Admin/products/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = await db.products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Admin/products/Create
        public ActionResult Create()
        {
            ViewBag.manufacturer_id = new SelectList(db.manufacturers, "id", "name");
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name");
            return View();
        }

        // POST: Admin/products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,manufacturer_id,size_chart_picture_id,description,name,price,quantity,creation_date")] product product)
        {
            if (ModelState.IsValid)
            {
                db.products.Add(product);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.manufacturer_id = new SelectList(db.manufacturers, "id", "name", product.manufacturer_id);
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product.size_chart_picture_id);
            return View(product);
        }

        // GET: Admin/products/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = await db.products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.manufacturer_id = new SelectList(db.manufacturers, "id", "name", product.manufacturer_id);
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product.size_chart_picture_id);
            return View(product);
        }

        // POST: Admin/products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,manufacturer_id,size_chart_picture_id,description,name,price,quantity,creation_date")] product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.manufacturer_id = new SelectList(db.manufacturers, "id", "name", product.manufacturer_id);
            ViewBag.size_chart_picture_id = new SelectList(db.pictures, "id", "name", product.size_chart_picture_id);
            return View(product);
        }

        // GET: Admin/products/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = await db.products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Admin/products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product product = await db.products.FindAsync(id);
            db.products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
