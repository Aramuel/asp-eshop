﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class option_valuesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/option_values
        public async Task<ActionResult> Index()
        {
            var option_values = db.option_values.Include(o => o.option);
            return View(await option_values.ToListAsync());
        }

        // GET: Admin/option_values/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            option_values option_values = await db.option_values.FindAsync(id);
            if (option_values == null)
            {
                return HttpNotFound();
            }
            return View(option_values);
        }

        // GET: Admin/option_values/Create
        public ActionResult Create()
        {
            ViewBag.option_types_id = new SelectList(db.options, "id", "value");
            return View();
        }

        // POST: Admin/option_values/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,option_types_id,value")] option_values option_values)
        {
            if (ModelState.IsValid)
            {
                db.option_values.Add(option_values);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.option_types_id = new SelectList(db.options, "id", "value", option_values.option_types_id);
            return View(option_values);
        }

        // GET: Admin/option_values/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            option_values option_values = await db.option_values.FindAsync(id);
            if (option_values == null)
            {
                return HttpNotFound();
            }
            ViewBag.option_types_id = new SelectList(db.options, "id", "value", option_values.option_types_id);
            return View(option_values);
        }

        // POST: Admin/option_values/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,option_types_id,value")] option_values option_values)
        {
            if (ModelState.IsValid)
            {
                db.Entry(option_values).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.option_types_id = new SelectList(db.options, "id", "value", option_values.option_types_id);
            return View(option_values);
        }

        // GET: Admin/option_values/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            option_values option_values = await db.option_values.FindAsync(id);
            if (option_values == null)
            {
                return HttpNotFound();
            }
            return View(option_values);
        }

        // POST: Admin/option_values/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            option_values option_values = await db.option_values.FindAsync(id);
            db.option_values.Remove(option_values);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
