﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class payment_methodsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/payment_methods
        public async Task<ActionResult> Index()
        {
            return View(await db.payment_methods.ToListAsync());
        }

        // GET: Admin/payment_methods/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment_methods payment_methods = await db.payment_methods.FindAsync(id);
            if (payment_methods == null)
            {
                return HttpNotFound();
            }
            return View(payment_methods);
        }

        // GET: Admin/payment_methods/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/payment_methods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,picture_id,name")] payment_methods payment_methods)
        {
            if (ModelState.IsValid)
            {
                db.payment_methods.Add(payment_methods);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(payment_methods);
        }

        // GET: Admin/payment_methods/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment_methods payment_methods = await db.payment_methods.FindAsync(id);
            if (payment_methods == null)
            {
                return HttpNotFound();
            }
            return View(payment_methods);
        }

        // POST: Admin/payment_methods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,picture_id,name")] payment_methods payment_methods)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payment_methods).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(payment_methods);
        }

        // GET: Admin/payment_methods/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment_methods payment_methods = await db.payment_methods.FindAsync(id);
            if (payment_methods == null)
            {
                return HttpNotFound();
            }
            return View(payment_methods);
        }

        // POST: Admin/payment_methods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            payment_methods payment_methods = await db.payment_methods.FindAsync(id);
            db.payment_methods.Remove(payment_methods);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
