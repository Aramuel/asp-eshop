﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class ordersController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/orders
        public async Task<ActionResult> Index()
        {
            var orders = db.orders.Include(o => o.address).Include(o => o.address1).Include(o => o.coupon).Include(o => o.shipping_types).Include(o => o.user).Include(o => o.payment_methods);
            return View(await orders.ToListAsync());
        }

        // GET: Admin/orders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Admin/orders/Create
        public ActionResult Create()
        {
            ViewBag.billing_address_id = new SelectList(db.addresses, "id", "street");
            ViewBag.shipping_address_id = new SelectList(db.addresses, "id", "street");
            ViewBag.coupon_id = new SelectList(db.coupons, "id", "name");
            ViewBag.shipping_type_id = new SelectList(db.shipping_types, "id", "description");
            ViewBag.user_id = new SelectList(db.users, "id", "email");
            ViewBag.payment_method_id = new SelectList(db.payment_methods, "id", "name");
            return View();
        }

        // POST: Admin/orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,user_id,coupon_id,shipping_type_id,shipping_address_id,billing_address_id,payment_method_id,total_price,vat,shipping_price")] order order)
        {
            if (ModelState.IsValid)
            {
                db.orders.Add(order);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.billing_address_id = new SelectList(db.addresses, "id", "street", order.billing_address_id);
            ViewBag.shipping_address_id = new SelectList(db.addresses, "id", "street", order.shipping_address_id);
            ViewBag.coupon_id = new SelectList(db.coupons, "id", "name", order.coupon_id);
            ViewBag.shipping_type_id = new SelectList(db.shipping_types, "id", "description", order.shipping_type_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", order.user_id);
            ViewBag.payment_method_id = new SelectList(db.payment_methods, "id", "name", order.payment_method_id);
            return View(order);
        }

        // GET: Admin/orders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.billing_address_id = new SelectList(db.addresses, "id", "street", order.billing_address_id);
            ViewBag.shipping_address_id = new SelectList(db.addresses, "id", "street", order.shipping_address_id);
            ViewBag.coupon_id = new SelectList(db.coupons, "id", "name", order.coupon_id);
            ViewBag.shipping_type_id = new SelectList(db.shipping_types, "id", "description", order.shipping_type_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", order.user_id);
            ViewBag.payment_method_id = new SelectList(db.payment_methods, "id", "name", order.payment_method_id);
            return View(order);
        }

        // POST: Admin/orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,user_id,coupon_id,shipping_type_id,shipping_address_id,billing_address_id,payment_method_id,total_price,vat,shipping_price")] order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.billing_address_id = new SelectList(db.addresses, "id", "street", order.billing_address_id);
            ViewBag.shipping_address_id = new SelectList(db.addresses, "id", "street", order.shipping_address_id);
            ViewBag.coupon_id = new SelectList(db.coupons, "id", "name", order.coupon_id);
            ViewBag.shipping_type_id = new SelectList(db.shipping_types, "id", "description", order.shipping_type_id);
            ViewBag.user_id = new SelectList(db.users, "id", "email", order.user_id);
            ViewBag.payment_method_id = new SelectList(db.payment_methods, "id", "name", order.payment_method_id);
            return View(order);
        }

        // GET: Admin/orders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Admin/orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            order order = await db.orders.FindAsync(id);
            db.orders.Remove(order);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
