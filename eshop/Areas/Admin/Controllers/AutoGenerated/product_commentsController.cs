﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_commentsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_comments
        public async Task<ActionResult> Index()
        {
            var product_comments = db.product_comments.Include(p => p.comment).Include(p => p.product);
            return View(await product_comments.ToListAsync());
        }

        // GET: Admin/product_comments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_comments product_comments = await db.product_comments.FindAsync(id);
            if (product_comments == null)
            {
                return HttpNotFound();
            }
            return View(product_comments);
        }

        // GET: Admin/product_comments/Create
        public ActionResult Create()
        {
            ViewBag.comment_id = new SelectList(db.comments, "id", "text");
            ViewBag.product_id = new SelectList(db.products, "id", "description");
            return View();
        }

        // POST: Admin/product_comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,comment_id")] product_comments product_comments)
        {
            if (ModelState.IsValid)
            {
                db.product_comments.Add(product_comments);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.comment_id = new SelectList(db.comments, "id", "text", product_comments.comment_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_comments.product_id);
            return View(product_comments);
        }

        // GET: Admin/product_comments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_comments product_comments = await db.product_comments.FindAsync(id);
            if (product_comments == null)
            {
                return HttpNotFound();
            }
            ViewBag.comment_id = new SelectList(db.comments, "id", "text", product_comments.comment_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_comments.product_id);
            return View(product_comments);
        }

        // POST: Admin/product_comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,comment_id")] product_comments product_comments)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_comments).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.comment_id = new SelectList(db.comments, "id", "text", product_comments.comment_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_comments.product_id);
            return View(product_comments);
        }

        // GET: Admin/product_comments/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_comments product_comments = await db.product_comments.FindAsync(id);
            if (product_comments == null)
            {
                return HttpNotFound();
            }
            return View(product_comments);
        }

        // POST: Admin/product_comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_comments product_comments = await db.product_comments.FindAsync(id);
            db.product_comments.Remove(product_comments);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
