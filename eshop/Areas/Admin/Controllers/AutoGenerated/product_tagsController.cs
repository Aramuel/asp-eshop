﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_tagsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_tags
        public async Task<ActionResult> Index()
        {
            var product_tags = db.product_tags.Include(p => p.product).Include(p => p.tag);
            return View(await product_tags.ToListAsync());
        }

        // GET: Admin/product_tags/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_tags product_tags = await db.product_tags.FindAsync(id);
            if (product_tags == null)
            {
                return HttpNotFound();
            }
            return View(product_tags);
        }

        // GET: Admin/product_tags/Create
        public ActionResult Create()
        {
            ViewBag.product_id = new SelectList(db.products, "id", "description");
            ViewBag.tag_id = new SelectList(db.tags, "id", "name");
            return View();
        }

        // POST: Admin/product_tags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,tag_id")] product_tags product_tags)
        {
            if (ModelState.IsValid)
            {
                db.product_tags.Add(product_tags);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.product_id = new SelectList(db.products, "id", "description", product_tags.product_id);
            ViewBag.tag_id = new SelectList(db.tags, "id", "name", product_tags.tag_id);
            return View(product_tags);
        }

        // GET: Admin/product_tags/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_tags product_tags = await db.product_tags.FindAsync(id);
            if (product_tags == null)
            {
                return HttpNotFound();
            }
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_tags.product_id);
            ViewBag.tag_id = new SelectList(db.tags, "id", "name", product_tags.tag_id);
            return View(product_tags);
        }

        // POST: Admin/product_tags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,tag_id")] product_tags product_tags)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_tags).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_tags.product_id);
            ViewBag.tag_id = new SelectList(db.tags, "id", "name", product_tags.tag_id);
            return View(product_tags);
        }

        // GET: Admin/product_tags/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_tags product_tags = await db.product_tags.FindAsync(id);
            if (product_tags == null)
            {
                return HttpNotFound();
            }
            return View(product_tags);
        }

        // POST: Admin/product_tags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_tags product_tags = await db.product_tags.FindAsync(id);
            db.product_tags.Remove(product_tags);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
