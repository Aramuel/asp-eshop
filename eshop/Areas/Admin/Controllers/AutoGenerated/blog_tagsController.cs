﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class blog_tagsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/blog_tags
        public async Task<ActionResult> Index()
        {
            var blog_tags = db.blog_tags.Include(b => b.tag).Include(b => b.blog);
            return View(await blog_tags.ToListAsync());
        }

        // GET: Admin/blog_tags/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_tags blog_tags = await db.blog_tags.FindAsync(id);
            if (blog_tags == null)
            {
                return HttpNotFound();
            }
            return View(blog_tags);
        }

        // GET: Admin/blog_tags/Create
        public ActionResult Create()
        {
            ViewBag.tag_id = new SelectList(db.tags, "id", "name");
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title");
            return View();
        }

        // POST: Admin/blog_tags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,blog_id,tag_id")] blog_tags blog_tags)
        {
            if (ModelState.IsValid)
            {
                db.blog_tags.Add(blog_tags);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.tag_id = new SelectList(db.tags, "id", "name", blog_tags.tag_id);
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_tags.blog_id);
            return View(blog_tags);
        }

        // GET: Admin/blog_tags/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_tags blog_tags = await db.blog_tags.FindAsync(id);
            if (blog_tags == null)
            {
                return HttpNotFound();
            }
            ViewBag.tag_id = new SelectList(db.tags, "id", "name", blog_tags.tag_id);
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_tags.blog_id);
            return View(blog_tags);
        }

        // POST: Admin/blog_tags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,blog_id,tag_id")] blog_tags blog_tags)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog_tags).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.tag_id = new SelectList(db.tags, "id", "name", blog_tags.tag_id);
            ViewBag.blog_id = new SelectList(db.blogs, "id", "title", blog_tags.blog_id);
            return View(blog_tags);
        }

        // GET: Admin/blog_tags/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog_tags blog_tags = await db.blog_tags.FindAsync(id);
            if (blog_tags == null)
            {
                return HttpNotFound();
            }
            return View(blog_tags);
        }

        // POST: Admin/blog_tags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            blog_tags blog_tags = await db.blog_tags.FindAsync(id);
            db.blog_tags.Remove(blog_tags);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
