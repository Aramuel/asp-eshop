﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_categoriesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_categories
        public async Task<ActionResult> Index()
        {
            var product_categories = db.product_categories.Include(p => p.category).Include(p => p.product);
            return View(await product_categories.ToListAsync());
        }

        // GET: Admin/product_categories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_categories product_categories = await db.product_categories.FindAsync(id);
            if (product_categories == null)
            {
                return HttpNotFound();
            }
            return View(product_categories);
        }

        // GET: Admin/product_categories/Create
        public ActionResult Create()
        {
            ViewBag.category_id = new SelectList(db.categories, "id", "name");
            ViewBag.product_id = new SelectList(db.products, "id", "description");
            return View();
        }

        // POST: Admin/product_categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,product_id,category_id")] product_categories product_categories)
        {
            if (ModelState.IsValid)
            {
                db.product_categories.Add(product_categories);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.category_id = new SelectList(db.categories, "id", "name", product_categories.category_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_categories.product_id);
            return View(product_categories);
        }

        // GET: Admin/product_categories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_categories product_categories = await db.product_categories.FindAsync(id);
            if (product_categories == null)
            {
                return HttpNotFound();
            }
            ViewBag.category_id = new SelectList(db.categories, "id", "name", product_categories.category_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_categories.product_id);
            return View(product_categories);
        }

        // POST: Admin/product_categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,product_id,category_id")] product_categories product_categories)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_categories).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.category_id = new SelectList(db.categories, "id", "name", product_categories.category_id);
            ViewBag.product_id = new SelectList(db.products, "id", "description", product_categories.product_id);
            return View(product_categories);
        }

        // GET: Admin/product_categories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_categories product_categories = await db.product_categories.FindAsync(id);
            if (product_categories == null)
            {
                return HttpNotFound();
            }
            return View(product_categories);
        }

        // POST: Admin/product_categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_categories product_categories = await db.product_categories.FindAsync(id);
            db.product_categories.Remove(product_categories);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
