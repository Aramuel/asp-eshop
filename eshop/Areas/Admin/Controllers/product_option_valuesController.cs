﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Areas.Admin.Controllers
{
    public class product_option_valuesController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Admin/product_option_values
        public async Task<ActionResult> Index()
        {
            var product_option_values = db.product_option_values.Include(p => p.option_values).Include(p => p.product_variants);
            return View(await product_option_values.ToListAsync());
        }

        // GET: Admin/product_option_values/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  
            }
            product_option_values product_option_values = await db.product_option_values.FindAsync(id); 
            if (product_option_values == null)
            {
                return HttpNotFound();
            }
            return View(product_option_values);
        }

        // GET: Admin/product_option_values/Create
        public ActionResult Create()
        {
            ViewBag.option_value_id = new SelectList(db.option_values, "id", "value");
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description");
            return View();
        }

        // POST: Admin/product_option_values/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,option_value_id,product_variant_id")] product_option_values product_option_values)
        {
            if (ModelState.IsValid)
            {
                db.product_option_values.Add(product_option_values);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.option_value_id = new SelectList(db.option_values, "id", "value", product_option_values.option_value_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_option_values.product_variant_id);
            return View(product_option_values);
        }

        // GET: Admin/product_option_values/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_option_values product_option_values = await db.product_option_values.FindAsync(id);
            if (product_option_values == null)
            {
                return HttpNotFound();
            }
            ViewBag.option_value_id = new SelectList(db.option_values, "id", "value", product_option_values.option_value_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_option_values.product_variant_id);
            return View(product_option_values);
        }

        // POST: Admin/product_option_values/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,option_value_id,product_variant_id")] product_option_values product_option_values)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_option_values).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.option_value_id = new SelectList(db.option_values, "id", "value", product_option_values.option_value_id);
            ViewBag.product_variant_id = new SelectList(db.product_variants, "id", "description", product_option_values.product_variant_id);
            return View(product_option_values);
        }

        // GET: Admin/product_option_values/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_option_values product_option_values = await db.product_option_values.FindAsync(id);
            if (product_option_values == null)
            {
                return HttpNotFound();
            }
            return View(product_option_values);
        }

        // POST: Admin/product_option_values/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            product_option_values product_option_values = await db.product_option_values.FindAsync(id);
            db.product_option_values.Remove(product_option_values);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
