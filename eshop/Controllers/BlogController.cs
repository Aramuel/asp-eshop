﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eshop.Controllers
{
    public class BlogController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: Blog
        public async Task<ActionResult> News()
        {
            /*
             List<v_blog_previews> blogs = await db.v_blog_previews.ToListAsync();
            return View(blogs.OrderBy(x => x.creation_date));
             */
            return View(await db.v_blog_previews.ToListAsync());
        }
        public async Task<ActionResult> Article(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //v_blog b = await db.v_blog.Where(x => x.id == id).FirstOrDefaultAsync();
            blog blog = await db.blogs.FindAsync(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }
    }
}