﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Eshop.Controllers
{
    public class HomeController : Controller
    {
        private eshopEntities db = new eshopEntities();
        
        public async Task<ActionResult> Index()
        {
            
            return View(await db.v_product_previews.ToListAsync());
        }
        public async Task<ActionResult> ProductDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variants product_variants = await db.product_variants.FindAsync(id);
            if (product_variants == null)
            {
                return HttpNotFound();
            }
            return View(product_variants);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}