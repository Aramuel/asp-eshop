﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Eshop;

namespace Eshop.Controllers.Default
{
    public class v_blog_previewsController : Controller
    {
        private eshopEntities db = new eshopEntities();

        // GET: v_blog_previews
        public async Task<ActionResult> Index()
        {
            return View(await db.v_blog_previews.ToListAsync());
        }

        // GET: v_blog_previews/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_blog_previews v_blog_previews = await db.v_blog_previews.FindAsync(id);
            if (v_blog_previews == null)
            {
                return HttpNotFound();
            }
            return View(v_blog_previews);
        }

        // GET: v_blog_previews/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: v_blog_previews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,title,author_fullname,preview,pictureName,picturePath,creation_date")] v_blog_previews v_blog_previews)
        {
            if (ModelState.IsValid)
            {
                db.v_blog_previews.Add(v_blog_previews);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(v_blog_previews);
        }

        // GET: v_blog_previews/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_blog_previews v_blog_previews = await db.v_blog_previews.FindAsync(id);
            if (v_blog_previews == null)
            {
                return HttpNotFound();
            }
            return View(v_blog_previews);
        }

        // POST: v_blog_previews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,title,author_fullname,preview,pictureName,picturePath,creation_date")] v_blog_previews v_blog_previews)
        {
            if (ModelState.IsValid)
            {
                db.Entry(v_blog_previews).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(v_blog_previews);
        }

        // GET: v_blog_previews/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_blog_previews v_blog_previews = await db.v_blog_previews.FindAsync(id);
            if (v_blog_previews == null)
            {
                return HttpNotFound();
            }
            return View(v_blog_previews);
        }

        // POST: v_blog_previews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            v_blog_previews v_blog_previews = await db.v_blog_previews.FindAsync(id);
            db.v_blog_previews.Remove(v_blog_previews);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
