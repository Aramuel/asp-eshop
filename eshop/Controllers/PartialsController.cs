﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eshop.Controllers.Default
{
    public class PartialsController : Controller
    {
        private eshopEntities db = new eshopEntities();
        [ChildActionOnly]
        public async Task<ActionResult> LatestBlogs()
        {
            return PartialView(await db.v_latest_blogs.ToListAsync());
        }
        public async Task<ActionResult> Archive()
        {
            return PartialView(await db.v_archive_blogs.ToListAsync());
        }
        public async Task<ActionResult> RecentBlogs()
        {
            return PartialView(await db.v_recent_posts.ToListAsync());
        }
        public async Task<ActionResult> ProductPreviews()
        {
            return PartialView(await db.v_product_previews.ToListAsync());
        }
    }
}