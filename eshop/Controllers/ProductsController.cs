﻿using Eshop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using PagedList;

namespace Eshop.Controllers
{
    public class ProductsController : Controller
    {
        private eshopEntities db = new eshopEntities();
        // GET: Products
        public async Task<ActionResult> Products(int? page)
        {
            var mn = await db.manufacturers.ToListAsync();
            var ct = await db.categories.ToListAsync();
            var op = await db.options.ToListAsync();
            var pp = await db.v_product_previews.Skip(() => 5 * (page ?? 1 - 1)).Take(() => 5).ToListAsync();

            ProductListing pf = new ProductListing(mn, op, ct);
            pf.product_previews = pp;

             //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?

            //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            //var onePageOfProducts = v_product_previews.ToPagedList(pageNumber, 5); // will only contain 25 products max because of the pageSize            

            return View(pf);
        }
        public async Task<ActionResult> ProductDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product_variants product_variants = await db.product_variants.FindAsync(id);
            if (product_variants == null)
            {
                return HttpNotFound();
            }
            return View(product_variants);
        }
    }
}