﻿using System.Web;
using System.Web.Optimization;

namespace Eshop
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/umd/popper.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-input-spinner.js"));

            bundles.Add(new ScriptBundle("~/bundles/sbadmin").Include(
                      "~/Scripts/sbadmin.js",
                      "~/Scripts/all.min.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/chart.min.js",
                      "~/Scripts/datatables.min.js",
                      "~/Scripts/demo/chart-area-demo.js",
                      "~/Scripts/demo/chart-bar-demo.js",
                      "~/Scripts/demo/chart-pie-demo.js",
                      "~/Scripts/demo/databases-demo.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css", 
                      "~/Content/font-awesome.css",
                      "~/Content/site.css"));
            
            bundles.Add(new StyleBundle("~/Content/admincss").Include(
                      "~/Content/sbadmin.css"
                      ));

        }
    }
}
