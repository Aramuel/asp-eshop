﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Eshop
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class eshopEntities : DbContext
    {
        public eshopEntities()
            : base("name=eshopEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<address> addresses { get; set; }
        public virtual DbSet<blog_comments> blog_comments { get; set; }
        public virtual DbSet<blog_tags> blog_tags { get; set; }
        public virtual DbSet<blog> blogs { get; set; }
        public virtual DbSet<category> categories { get; set; }
        public virtual DbSet<comment> comments { get; set; }
        public virtual DbSet<coupon> coupons { get; set; }
        public virtual DbSet<manufacturer> manufacturers { get; set; }
        public virtual DbSet<option_values> option_values { get; set; }
        public virtual DbSet<option> options { get; set; }
        public virtual DbSet<order_product_variants> order_product_variants { get; set; }
        public virtual DbSet<order> orders { get; set; }
        public virtual DbSet<payment_methods> payment_methods { get; set; }
        public virtual DbSet<picture> pictures { get; set; }
        public virtual DbSet<product_categories> product_categories { get; set; }
        public virtual DbSet<product_comments> product_comments { get; set; }
        public virtual DbSet<product_options> product_options { get; set; }
        public virtual DbSet<product_sales> product_sales { get; set; }
        public virtual DbSet<product_tags> product_tags { get; set; }
        public virtual DbSet<product_variant_pictures> product_variant_pictures { get; set; }
        public virtual DbSet<product_variants> product_variants { get; set; }
        public virtual DbSet<product> products { get; set; }
        public virtual DbSet<question> questions { get; set; }
        public virtual DbSet<review> reviews { get; set; }
        public virtual DbSet<sale> sales { get; set; }
        public virtual DbSet<shipping_types> shipping_types { get; set; }
        public virtual DbSet<tag> tags { get; set; }
        public virtual DbSet<user_shipping_adresses> user_shipping_adresses { get; set; }
        public virtual DbSet<user_wishlists> user_wishlists { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<wishlist_product_variants> wishlist_product_variants { get; set; }
        public virtual DbSet<wishlist> wishlists { get; set; }
        public virtual DbSet<v_archive_blogs> v_archive_blogs { get; set; }
        public virtual DbSet<v_blog_previews> v_blog_previews { get; set; }
        public virtual DbSet<v_latest_blogs> v_latest_blogs { get; set; }
        public virtual DbSet<v_recent_posts> v_recent_posts { get; set; }
        public virtual DbSet<v_product_previews> v_product_previews { get; set; }
        public virtual DbSet<v_blog> v_blog { get; set; }
        public virtual DbSet<product_option_values> product_option_values { get; set; }
    }
}
